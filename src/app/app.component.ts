import { AfterViewInit, Component, OnInit } from '@angular/core';
import { LOGO_ITEMS } from '../assets/items/logo-items';
import { BANNER_ITEMS } from '../assets/items/banner-items';
import { FLYER_ITEMS } from '../assets/items/flyer-items';
import { MAQUILA_ITEMS } from '../assets/items/maquila-items';


declare var $: any;

export interface IItem {
  categoryClass: string;
  imgSrc: string;
  imgBigSrc?: string;
  description: string;
}

export function shuffle(array) {
  return array.sort(() => Math.random() - 0.5);
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, AfterViewInit {
  logoItems = LOGO_ITEMS;
  bannerItems = BANNER_ITEMS;
  flyerItems = FLYER_ITEMS;
  maquilaItems = MAQUILA_ITEMS;

  totalItems: IItem[] = [];

  constructor() {
  }

  ngOnInit(): void {
    this.totalItems = [
      ...this.logoItems,
      ...this.bannerItems,
      ...this.flyerItems,
      ...this.maquilaItems,
    ];

    this.totalItems = shuffle(this.totalItems);
  }


  ngAfterViewInit(): void {

    // initialization of svg injector module
    $.HSCore.components.HSSVGIngector.init('.js-svg-injector');

    // initialization of header
    // $.HSCore.components.HSHeader.init($('#header'));

    // initialization of malihu scrollbar
    // $.HSCore.components.HSMalihuScrollBar.init($('.js-scrollbar'));

    // initialization of forms
    // $.HSCore.components.HSFocusState.init();

    // initialization of cubeportfolio
    $.HSCore.components.HSCubeportfolio.init('.cbp');

    // initialization of go to
    $.HSCore.components.HSGoTo.init('.js-go-to');

    const items = $.HSCore.components.HSChartPie.init('.js-pie');
  }

}
