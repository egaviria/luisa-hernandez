import { IItem } from '../../app/app.component';

export const BANNER_ITEMS: IItem[] = [
  {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner1.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner1.jpg',
    description: ''
  },
    {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner2.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner2.jpg',
    description: ''
  },
    {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner3.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner3.jpg',
    description: ''
  },
    {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner4.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner4.jpg',
    description: ''
  },
    {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner5.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner5.jpg',
    description: ''
  },
    {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner6.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner6.jpg',
    description: ''
  },
    {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner7.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner7.jpg',
    description: ''
  },
    {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner8.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner8.jpg',
    description: ''
  },
    {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner9.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner9.jpg',
    description: ''
  },
    {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner10.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner10.jpg',
    description: ''
  },
    {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner11.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner11.jpg',
    description: ''
  },
    {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner12.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner12.jpg',
    description: ''
  },
    {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner13.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner13.jpg',
    description: ''
  },
    {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner14.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner14.jpg',
    description: ''
  },
    {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner15.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner15.jpg',
    description: ''
  },
    {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner16.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner16.jpg',
    description: ''
  },
    {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner17.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner17.jpg',
    description: ''
  },
    {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner18.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner18.jpg',
    description: ''
  },
    {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner19.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner19.jpg',
    description: ''
  },
    {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner20.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner20.jpg',
    description: ''
  },
    {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner21.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner21.jpg',
    description: ''
  },
    {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner22.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner22.jpg',
    description: ''
  },
    {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner23.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner23.jpg',
    description: ''
  },
    {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner24.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner24.jpg',
    description: ''
  },
    {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner25.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner25.jpg',
    description: ''
  },
    {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner26.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner26.jpg',
    description: ''
  },
    {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner27.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner27.jpg',
    description: ''
  },
    {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner28.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner28.jpg',
    description: ''
  },
    {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner29.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner29.jpg',
    description: ''
  },
    {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner30.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner30.jpg',
    description: ''
  },
    {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner31.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner31.jpg',
    description: ''
  },
    {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner32.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner32.jpg',
    description: ''
  },
    {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner33.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner33.jpg',
    description: ''
  },
    {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner34.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner34.jpg',
    description: ''
  },
    {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner35.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner35.jpg',
    description: ''
  },
    {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner36.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner36.jpg',
    description: ''
  },
    {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner37.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner37.jpg',
    description: ''
  },
    {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner38.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner38.jpg',
    description: ''
  },
    {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner39.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner39.jpg',
    description: ''
  },
    {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner40.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner40.jpg',
    description: ''
  },
    {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner41.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner41.jpg',
    description: ''
  },
    {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner42.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner42.jpg',
    description: ''
  },
    {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner43.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner43.jpg',
    description: ''
  },
    {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner44.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner44.jpg',
    description: ''
  },
    {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner45.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner45.jpg',
    description: ''
  },
    {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner46.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner46.jpg',
    description: ''
  },
    {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner47.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner47.jpg',
    description: ''
  },
    {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner48.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner48.jpg',
    description: ''
  },
    {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner49.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner49.jpg',
    description: ''
  },
    {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner50.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner50.jpg',
    description: ''
  },
    {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner51.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner51.jpg',
    description: ''
  },
    {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner52.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner52.jpg',
    description: ''
  },
    {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner53.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner53.jpg',
    description: ''
  },
    {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner54.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner54.jpg',
    description: ''
  },
    {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner55.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner55.jpg',
    description: ''
  },
    {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner56.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner56.jpg',
    description: ''
  },
    {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner57.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner57.jpg',
    description: ''
  },
    {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner58.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner58.jpg',
    description: ''
  },
    {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner59.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner59.jpg',
    description: ''
  },
    {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner60.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner60.jpg',
    description: ''
  },
    {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner61.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner61.jpg',
    description: ''
  },
    {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner62.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner62.jpg',
    description: ''
  },
    {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner63.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner63.jpg',
    description: ''
  },
    {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner64.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner64.jpg',
    description: ''
  },
    {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner65.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner65.jpg',
    description: ''
  },
    {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner66.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner66.jpg',
    description: ''
  },
    {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner67.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner67.jpg',
    description: ''
  },
    {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner68.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner68.jpg',
    description: ''
  },
    {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner69.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner69.jpg',
    description: ''
  },

  {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner70.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner70.jpg',
    description: ''
  },
  {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner71.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner71.jpg',
    description: ''
  },
  {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner72.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner72.jpg',
    description: ''
  },
  {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner73.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner73.jpg',
    description: ''
  },
  {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner74.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner74.jpg',
    description: ''
  },
  {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner75.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner75.jpg',
    description: ''
  },
  {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner76.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner76.jpg',
    description: ''
  },
  {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner77.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner77.jpg',
    description: ''
  },
  {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner78.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner78.jpg',
    description: ''
  },
  {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner79.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner79.jpg',
    description: ''
  },
  {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner80.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner80.jpg',
    description: ''
  },
  {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner81.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner81.jpg',
    description: ''
  },
  {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner82.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner82.jpg',
    description: ''
  },
  {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner83.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner83.jpg',
    description: ''
  },
  {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner84.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner84.jpg',
    description: ''
  },
  {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner85.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner85.jpg',
    description: ''
  },
  {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner86.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner86.jpg',
    description: ''
  },
  {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner87.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner87.jpg',
    description: ''
  },
  {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner88.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner88.jpg',
    description: ''
  },
  {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner89.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner89.jpg',
    description: ''
  },
  {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner90.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner90.jpg',
    description: ''
  },
  {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner91.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner91.jpg',
    description: ''
  },
  {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner92.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner92.jpg',
    description: ''
  },
  {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner93.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner93.jpg',
    description: ''
  },
  {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner94.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner94.jpg',
    description: ''
  },
  {
    categoryClass: 'banner',
    imgSrc: './assets/img-portafolio/banner/banner95.jpg',
    imgBigSrc: './assets/img-portafolio/bannerBig/banner95.jpg',
    description: ''
  }
];
