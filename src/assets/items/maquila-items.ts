import { IItem } from '../../app/app.component';

export const MAQUILA_ITEMS: IItem[] = [
  {
    categoryClass: 'maquila',
    imgSrc: './assets/img-portafolio/maquilas/maquila1.jpg',
    imgBigSrc: './assets/img-portafolio/maquilasBig/maquila1.jpg',
    description: ''
  },
  {
    categoryClass: 'maquila',
    imgSrc: './assets/img-portafolio/maquilas/maquila2.jpg',
    imgBigSrc: './assets/img-portafolio/maquilasBig/maquila2.jpg',
    description: ''
  },
  {
    categoryClass: 'maquila',
    imgSrc: './assets/img-portafolio/maquilas/maquila3.jpg',
    imgBigSrc: './assets/img-portafolio/maquilasBig/maquila3.jpg',
    description: ''
  },
  {
    categoryClass: 'maquila',
    imgSrc: './assets/img-portafolio/maquilas/maquila4.jpg',
    imgBigSrc: './assets/img-portafolio/maquilasBig/maquila4.jpg',
    description: ''
  },
  {
    categoryClass: 'maquila',
    imgSrc: './assets/img-portafolio/maquilas/maquila5.jpg',
    imgBigSrc: './assets/img-portafolio/maquilasBig/maquila5.jpg',
    description: ''
  },
  {
    categoryClass: 'maquila',
    imgSrc: './assets/img-portafolio/maquilas/maquila6.jpg',
    imgBigSrc: './assets/img-portafolio/maquilasBig/maquila6.jpg',
    description: ''
  },
  {
    categoryClass: 'maquila',
    imgSrc: './assets/img-portafolio/maquilas/maquila7.jpg',
    imgBigSrc: './assets/img-portafolio/maquilasBig/maquila7.jpg',
    description: ''
  },
  {
    categoryClass: 'maquila',
    imgSrc: './assets/img-portafolio/maquilas/maquila8.jpg',
    imgBigSrc: './assets/img-portafolio/maquilasBig/maquila8.jpg',
    description: ''
  },
  {
    categoryClass: 'maquila',
    imgSrc: './assets/img-portafolio/maquilas/maquila9.jpg',
    imgBigSrc: './assets/img-portafolio/maquilasBig/maquila9.jpg',
    description: ''
  },
  {
    categoryClass: 'maquila',
    imgSrc: './assets/img-portafolio/maquilas/maquila10.jpg',
    imgBigSrc: './assets/img-portafolio/maquilasBig/maquila10.jpg',
    description: ''
  },
  {
    categoryClass: 'maquila',
    imgSrc: './assets/img-portafolio/maquilas/maquila11.jpg',
    imgBigSrc: './assets/img-portafolio/maquilasBig/maquila11.jpg',
    description: ''
  },
  {
    categoryClass: 'maquila',
    imgSrc: './assets/img-portafolio/maquilas/maquila12.jpg',
    imgBigSrc: './assets/img-portafolio/maquilasBig/maquila12.jpg',
    description: ''
  },
  {
    categoryClass: 'maquila',
    imgSrc: './assets/img-portafolio/maquilas/maquila13.jpg',
    imgBigSrc: './assets/img-portafolio/maquilasBig/maquila13.jpg',
    description: ''
  },
  {
    categoryClass: 'maquila',
    imgSrc: './assets/img-portafolio/maquilas/maquila14.jpg',
    imgBigSrc: './assets/img-portafolio/maquilasBig/maquila14.jpg',
    description: ''
  },
  {
    categoryClass: 'maquila',
    imgSrc: './assets/img-portafolio/maquilas/maquila15.jpg',
    imgBigSrc: './assets/img-portafolio/maquilasBig/maquila15.jpg',
    description: ''
  },
  {
    categoryClass: 'maquila',
    imgSrc: './assets/img-portafolio/maquilas/maquila16.jpg',
    imgBigSrc: './assets/img-portafolio/maquilasBig/maquila16.jpg',
    description: ''
  },
  {
    categoryClass: 'maquila',
    imgSrc: './assets/img-portafolio/maquilas/maquila17.jpg',
    imgBigSrc: './assets/img-portafolio/maquilasBig/maquila17.jpg',
    description: ''
  },
  {
    categoryClass: 'maquila',
    imgSrc: './assets/img-portafolio/maquilas/maquila18.jpg',
    imgBigSrc: './assets/img-portafolio/maquilasBig/maquila18.jpg',
    description: ''
  },
  {
    categoryClass: 'maquila',
    imgSrc: './assets/img-portafolio/maquilas/maquila19.jpg',
    imgBigSrc: './assets/img-portafolio/maquilasBig/maquila19.jpg',
    description: ''
  }
];
