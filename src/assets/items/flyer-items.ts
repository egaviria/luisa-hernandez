import { IItem } from '../../app/app.component';

export const FLYER_ITEMS: IItem[] = [
  {
    categoryClass: 'flyer',
    imgSrc: './assets/img-portafolio/flyers/flyer1.jpg',
    imgBigSrc: './assets/img-portafolio/flyersBig/flyer1.jpg',
    description: ''
  },
  {
    categoryClass: 'flyer',
    imgSrc: './assets/img-portafolio/flyers/flyer3.jpg',
    imgBigSrc: './assets/img-portafolio/flyersBig/flyer3.jpg',
    description: ''
  },
  {
    categoryClass: 'flyer',
    imgSrc: './assets/img-portafolio/flyers/flyer4.jpg',
    imgBigSrc: './assets/img-portafolio/flyersBig/flyer4.jpg',
    description: ''
  },
  {
    categoryClass: 'flyer',
    imgSrc: './assets/img-portafolio/flyers/flyer5.jpg',
    imgBigSrc: './assets/img-portafolio/flyersBig/flyer5.jpg',
    description: ''
  },
  {
    categoryClass: 'flyer',
    imgSrc: './assets/img-portafolio/flyers/flyer7.jpg',
    imgBigSrc: './assets/img-portafolio/flyersBig/flyer7.jpg',
    description: ''
  },
  {
    categoryClass: 'flyer',
    imgSrc: './assets/img-portafolio/flyers/flyer8.jpg',
    imgBigSrc: './assets/img-portafolio/flyersBig/flyer8.jpg',
    description: ''
  },
  {
    categoryClass: 'flyer',
    imgSrc: './assets/img-portafolio/flyers/flyer9.jpg',
    imgBigSrc: './assets/img-portafolio/flyersBig/flyer9.jpg',
    description: ''
  },
  {
    categoryClass: 'flyer',
    imgSrc: './assets/img-portafolio/flyers/flyer10.jpg',
    imgBigSrc: './assets/img-portafolio/flyersBig/flyer10.jpg',
    description: ''
  },
  {
    categoryClass: 'flyer',
    imgSrc: './assets/img-portafolio/flyers/flyer11.jpg',
    imgBigSrc: './assets/img-portafolio/flyersBig/flyer11.jpg',
    description: ''
  },
  {
    categoryClass: 'flyer',
    imgSrc: './assets/img-portafolio/flyers/flyer12.jpg',
    imgBigSrc: './assets/img-portafolio/flyersBig/flyer12.jpg',
    description: ''
  },
  {
    categoryClass: 'flyer',
    imgSrc: './assets/img-portafolio/flyers/flyer14.jpg',
    imgBigSrc: './assets/img-portafolio/flyersBig/flyer14.jpg',
    description: ''
  },
  {
    categoryClass: 'flyer',
    imgSrc: './assets/img-portafolio/flyers/flyer15.jpg',
    imgBigSrc: './assets/img-portafolio/flyersBig/flyer15.jpg',
    description: ''
  },
  {
    categoryClass: 'flyer',
    imgSrc: './assets/img-portafolio/flyers/flyer16.jpg',
    imgBigSrc: './assets/img-portafolio/flyersBig/flyer16.jpg',
    description: ''
  },
  {
    categoryClass: 'flyer',
    imgSrc: './assets/img-portafolio/flyers/flyer17.jpg',
    imgBigSrc: './assets/img-portafolio/flyersBig/flyer17.jpg',
    description: ''
  },
  {
    categoryClass: 'flyer',
    imgSrc: './assets/img-portafolio/flyers/flyer18.jpg',
    imgBigSrc: './assets/img-portafolio/flyersBig/flyer18.jpg',
    description: ''
  },
  {
    categoryClass: 'flyer',
    imgSrc: './assets/img-portafolio/flyers/flyer19.jpg',
    imgBigSrc: './assets/img-portafolio/flyersBig/flyer19.jpg',
    description: ''
  },
  {
    categoryClass: 'flyer',
    imgSrc: './assets/img-portafolio/flyers/flyer20.jpg',
    imgBigSrc: './assets/img-portafolio/flyersBig/flyer20.jpg',
    description: ''
  },
  {
    categoryClass: 'flyer',
    imgSrc: './assets/img-portafolio/flyers/flyer21.jpg',
    imgBigSrc: './assets/img-portafolio/flyersBig/flyer21.jpg',
    description: ''
  },
  {
    categoryClass: 'flyer',
    imgSrc: './assets/img-portafolio/flyers/flyer22.jpg',
    imgBigSrc: './assets/img-portafolio/flyersBig/flyer22.jpg',
    description: ''
  },
  {
    categoryClass: 'flyer',
    imgSrc: './assets/img-portafolio/flyers/flyer23.jpg',
    imgBigSrc: './assets/img-portafolio/flyersBig/flyer23.jpg',
    description: ''
  },
  {
    categoryClass: 'flyer',
    imgSrc: './assets/img-portafolio/flyers/flyer24.jpg',
    imgBigSrc: './assets/img-portafolio/flyersBig/flyer24.jpg',
    description: ''
  },

  {
    categoryClass: 'flyer',
    imgSrc: './assets/img-portafolio/flyers/flyer25.jpg',
    imgBigSrc: './assets/img-portafolio/flyersBig/flyer25.jpg',
    description: ''
  },
  {
    categoryClass: 'flyer',
    imgSrc: './assets/img-portafolio/flyers/flyer26.jpg',
    imgBigSrc: './assets/img-portafolio/flyersBig/flyer26.jpg',
    description: ''
  },
  {
    categoryClass: 'flyer',
    imgSrc: './assets/img-portafolio/flyers/flyer27.jpg',
    imgBigSrc: './assets/img-portafolio/flyersBig/flyer27.jpg',
    description: ''
  },
  {
    categoryClass: 'flyer',
    imgSrc: './assets/img-portafolio/flyers/flyer28.jpg',
    imgBigSrc: './assets/img-portafolio/flyersBig/flyer28.jpg',
    description: ''
  },
  {
    categoryClass: 'flyer',
    imgSrc: './assets/img-portafolio/flyers/flyer29.jpg',
    imgBigSrc: './assets/img-portafolio/flyersBig/flyer29.jpg',
    description: ''
  },
  {
    categoryClass: 'flyer',
    imgSrc: './assets/img-portafolio/flyers/flyer30.jpg',
    imgBigSrc: './assets/img-portafolio/flyersBig/flyer30.jpg',
    description: ''
  },
  {
    categoryClass: 'flyer',
    imgSrc: './assets/img-portafolio/flyers/flyer31.jpg',
    imgBigSrc: './assets/img-portafolio/flyersBig/flyer31.jpg',
    description: ''
  },
  {
    categoryClass: 'flyer',
    imgSrc: './assets/img-portafolio/flyers/flyer32.jpg',
    imgBigSrc: './assets/img-portafolio/flyersBig/flyer32.jpg',
    description: ''
  }
];
