import { IItem } from '../../app/app.component';

export const LOGO_ITEMS: IItem[] = [
  {
    categoryClass: 'logo',
    imgSrc: './assets/img-portafolio/logos/logo1.jpg',
    imgBigSrc: './assets/img-portafolio/logosBig/logo1.jpg',
    description: 'Hotel Bella vista'
  }, {
    categoryClass: 'logo',
    imgSrc: './assets/img-portafolio/logos/logo2.jpg',
    imgBigSrc: './assets/img-portafolio/logosBig/logo2.jpg',
    description: 'Empresa IBG'
  }, {
    categoryClass: 'logo',
    imgSrc: './assets/img-portafolio/logos/logo3.jpg',
    imgBigSrc: './assets/img-portafolio/logosBig/logo3.jpg',
    description: 'Constructora'
  }, {
    categoryClass: 'logo',
    imgSrc: './assets/img-portafolio/logos/logo4.jpg',
    imgBigSrc: './assets/img-portafolio/logosBig/logo4.jpg',
    description: 'Marca de accesorios'
  }, {
    categoryClass: 'logo',
    imgSrc: './assets/img-portafolio/logos/logo5.jpg',
    imgBigSrc: './assets/img-portafolio/logosBig/logo5.jpg',
    description: 'Pagina Web'
  }, {
    categoryClass: 'logo',
    imgSrc: './assets/img-portafolio/logos/logo6.jpg',
    imgBigSrc: './assets/img-portafolio/logosBig/logo6.jpg',
    description: 'Constructora'
  }, {
    categoryClass: 'logo',
    imgSrc: './assets/img-portafolio/logos/logo7.jpg',
    imgBigSrc: './assets/img-portafolio/logosBig/logo7.jpg',
    description: 'Image Description'
  }, {
    categoryClass: 'logo',
    imgSrc: './assets/img-portafolio/logos/logo8.jpg',
    imgBigSrc: './assets/img-portafolio/logosBig/logo8.jpg',
    description: 'Marca de accesorios'
  }, {
    categoryClass: 'logo',
    imgSrc: './assets/img-portafolio/logos/logo9.jpg',
    imgBigSrc: './assets/img-portafolio/logosBig/logo9.jpg',
    description: 'Casa cultural'
  }, {
    categoryClass: 'logo',
    imgSrc: './assets/img-portafolio/logos/logo10.jpg',
    imgBigSrc: './assets/img-portafolio/logosBig/logo10.jpg',
    description: 'Página Web'
  }, {
    categoryClass: 'logo',
    imgSrc: './assets/img-portafolio/logos/logo11.jpg',
    imgBigSrc: './assets/img-portafolio/logosBig/logo11.jpg',
    description: 'Mascota centro comercial'
  }, {
    categoryClass: 'logo',
    imgSrc: './assets/img-portafolio/logos/logo12.jpg',
    imgBigSrc: './assets/img-portafolio/logosBig/logo12.jpg',
    description: 'Productos de aseo'
  }, {
    categoryClass: 'logo',
    imgSrc: './assets/img-portafolio/logos/logo13.jpg',
    imgBigSrc: './assets/img-portafolio/logosBig/logo13.jpg',
    description: 'Accesorios & Joyería'
  }, {
    categoryClass: 'logo',
    imgSrc: './assets/img-portafolio/logos/logo14.jpg',
    imgBigSrc: './assets/img-portafolio/logosBig/logo14.jpg',
    description: 'Fonda'
  }, {
    categoryClass: 'logo',
    imgSrc: './assets/img-portafolio/logos/logo15.jpg',
    imgBigSrc: './assets/img-portafolio/logosBig/logo15.jpg',
    description: 'Hotel Copacabana'
  }, {
    categoryClass: 'logo',
    imgSrc: './assets/img-portafolio/logos/logo16.jpg',
    imgBigSrc: './assets/img-portafolio/logosBig/logo16.jpg',
    description: 'Productos de aseo'
  },

  {
    categoryClass: 'logo',
    imgSrc: './assets/img-portafolio/logos/logo17.jpg',
    imgBigSrc: './assets/img-portafolio/logosBig/logo17.jpg',
    description: 'Productos de aseo'
  }, {
    categoryClass: 'logo',
    imgSrc: './assets/img-portafolio/logos/logo18.jpg',
    imgBigSrc: './assets/img-portafolio/logosBig/logo18.jpg',
    description: 'Productos de aseo'
  },
];
